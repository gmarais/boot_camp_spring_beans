package za.co.boot.camp.bean.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.boot.camp.bean.repository.AccountRepository;
import za.co.boot.camp.bean.repository.CustomerRepository;

@Service
public class DashboardService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DashboardService{");
        sb.append("accountRepository=").append(accountRepository);
        sb.append(", customerRepository=").append(customerRepository);
        sb.append('}');
        return sb.toString();
    }
}