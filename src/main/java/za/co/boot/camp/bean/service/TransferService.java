package za.co.boot.camp.bean.service;

import za.co.boot.camp.bean.repository.AccountRepository;
import za.co.boot.camp.bean.repository.CustomerRepository;

public class TransferService {

    private AccountRepository accountRepository;
    private CustomerRepository customerRepository;

    public TransferService() {
    }

    public TransferService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TransferService{");
        sb.append("accountRepository=").append(accountRepository);
        sb.append(", customerRepository=").append(customerRepository);
        sb.append('}');
        return sb.toString();
    }
}
