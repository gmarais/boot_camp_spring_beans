package za.co.boot.camp.bean.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import za.co.boot.camp.bean.service.DashboardService;

@Controller
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DashboardController{");
        sb.append("dashboardService=").append(dashboardService);
        sb.append('}');
        return sb.toString();
    }
}