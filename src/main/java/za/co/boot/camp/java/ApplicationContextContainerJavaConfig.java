package za.co.boot.camp.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.boot.camp.bean.service.TransferService;

public class ApplicationContextContainerJavaConfig {

    public static void main(String[] args) {
        createJavaConfig();
    }

    private static void createJavaConfig(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppJavaConfig.class);
        TransferService transferService = context.getBean("transferService",  TransferService.class);
        System.out.println(transferService);
    }
}