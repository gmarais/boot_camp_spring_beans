package za.co.boot.camp.java;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.boot.camp.bean.repository.AccountRepository;
import za.co.boot.camp.bean.repository.CustomerRepository;
import za.co.boot.camp.bean.service.TransferService;

@Configuration
public class AppJavaConfig {

    @Bean
    public TransferService transferService(AccountRepository accountRepository, CustomerRepository customerRepository){
        TransferService transferService = new TransferService(customerRepository);
        transferService.setAccountRepository(accountRepository);
        return transferService;
    }

    @Bean
    public AccountRepository accountRepository(){
        return new AccountRepository();
    }

    @Bean
    public CustomerRepository customerRepository(){
        return new CustomerRepository();
    }
}