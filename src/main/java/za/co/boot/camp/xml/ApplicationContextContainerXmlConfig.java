package za.co.boot.camp.xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import za.co.boot.camp.bean.service.TransferService;

public class ApplicationContextContainerXmlConfig {

    public static void main(String[] args) {
        createXmlContext();
    }

    private static void createXmlContext() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        TransferService transferService = context.getBean("transferService", TransferService.class);
        System.out.println(transferService);
    }
}