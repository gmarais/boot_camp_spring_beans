package za.co.boot.camp.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import za.co.boot.camp.bean.controller.DashboardController;

@ComponentScan("za.co.boot.camp.bean")
public class ApplicationContextContainerAnnotationConfig {

    public static void main(String[] args) {
        createAnnotationConfig();
    }

    private static void createAnnotationConfig(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextContainerAnnotationConfig.class);
        DashboardController dashboardController = context.getBean("dashboardController", DashboardController.class);
        System.out.println(dashboardController);
    }
}