###Exercise 1: Create a transferService bean using xml config 

Branch : exercise_1_and_2

The class ApplicationContextContainerXmlConfig instantiates an xml application context that loads the config file 
"applicationContext.xml". 

Add a bean definition to the "applicationContext.xml" file so that the line context.getBean("transferService")
returns a instance of ***TransferService***

###Exercise 2: Create a transferService bean using java config 

Branch : exercise_1_and_2

The class ApplicationContextContainerJavaConfig instantiates an application context configured with annotations. 

Add a bean definition to the AppConfig class so that the line context.getBean("transferService") returns a instance of ***TransferService***

###Exercise 3: Inject transferService bean dependencies using xml config

Branch : exercise_3_and_4

The previous exercises created a transferService bean, however the accountRepository and customerRepository defined in
the transferService are null.

Update the "applicationContext.xml" to inject these beans into the transferService using constructor and setter injection.

###Exercise 4: Inject transferService bean dependencies using java config

Branch : exercise_3_and_4

Same as exercise 3, but update AppConfig to inject the accountRepository and customerRepository into transferService using 
java config.

###Exercise 5: Inject DashboardController dependencies using annotation config

Branch : exercise_5

DashboardController has a dependency on DashboardService which in turn depends on accountRepository and customerRepository 
Add additional annotations to create beans of these classes and to Autowire these dependencies.

Run ApplicationContextContainerAnnotationConfig to confirm injection is working correctly.